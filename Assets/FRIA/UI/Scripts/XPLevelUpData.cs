﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class XPLevelUpData 
{

    public int lvl_i;
    public float xp_i;
    public float xp_i_max;

    public int lvl_f;
    public float xp_f;
    public float xp_f_max;

    public XPLevelUpData() { }
    public XPLevelUpData(int i_lvl, float i_xp, float i_xpmax)
    {
        lvl_i = i_lvl;
        xp_i = i_xp;
        xp_i_max = i_xpmax;

    }

}
