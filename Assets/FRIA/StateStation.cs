﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class StateStation<T>
{
    public T currentState
    {
        get { return _current; } private set{_current = value;}
    }

    [SerializeField] T _current;

    public StateStation(T initial)
    {
        currentState = initial;
    }

    public event Action<T, T> onTransition;
    public Dictionary<T, Dictionary<T,System.Action>> transitions = new Dictionary<T, Dictionary<T, System.Action>>();

    public void Transition(T fromState, T toState, Action onTransition)
    {
        Dictionary<T, Action> fromTransitions = null;
        if (!transitions.ContainsKey(fromState))
        {
            fromTransitions = new Dictionary<T, Action>();
            transitions.Add(fromState, fromTransitions);
        }
        else
        {
            fromTransitions = transitions[fromState];
        }

        if (fromTransitions.ContainsKey(toState))
        {
            fromTransitions[toState] += onTransition;
        }
        else
        {
            fromTransitions.Add(toState,onTransition);
        }
    }
    public void Set(T newState)
    {
        if (transitions.ContainsKey(currentState))
        {
            Dictionary<T, System.Action> fromCurrent = transitions[currentState];

            if (fromCurrent.ContainsKey(newState))
            {
                T oldstate = currentState;
                currentState = newState;
                fromCurrent[newState]?.Invoke();
                onTransition?.Invoke(currentState,newState);
            }
            else
            {
                Debug.LogErrorFormat("Transition {0} => {1} not defined",currentState, newState);
            }
        }
        else
        {
            Debug.LogErrorFormat("Transition {0} => {1} not defined", currentState, newState);
        }
    }
}

