﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollRepeater : MonoBehaviour
{
    public Vector3 scrollVelocity = new Vector3(-1,0,0);
    public float repeatLength = 100;
    public Transform scrollTarget;
    [Header("Spec, Dont Edit")]
    [SerializeField] Vector3 displacement;
    [SerializeField] Vector3 initialPosition;

    private void Start()
    {
        initialPosition = scrollTarget.position;
    }
    void Update()
    {
        if (scrollTarget)
        {
            displacement += scrollVelocity * Time.deltaTime;
            float dispMag = displacement.magnitude;
            if (dispMag > repeatLength)
            {
                displacement = (displacement / dispMag) * (dispMag-repeatLength);
            }
            scrollTarget.position = initialPosition + displacement;
        }
    }
}
