﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBox : MonoBehaviour
{
    public Transform target;


    // Update is called once per frame
    void Update()
    {
        this.transform.position = target.transform.position;
        this.transform.rotation = target.transform.rotation;
        //this.transform.eulerAngles = new Vector3(0, target.transform.rotation.eulerAngles.y,0);
    }
}
