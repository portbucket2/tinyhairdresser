﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TexturePaint2D
{

    public Material mat;
    public int dimention = 256;
    public float brushSize_256 = 20;

    public Color32 startColor = new Color32(1,1,1,1);
    public Color32 paintedColor = new Color32(0,0,0,0);

    public string mapName = "_ScalpMap";

    [Header("Spec")]
    public Vector2Int N;
    public Texture2D tex2D;
    internal Color32[] cols2D;

    public void Init()
    {
        N = new Vector2Int(dimention,dimention);
        cols2D = new Color32[N.x * N.y];
        tex2D = new Texture2D(N.x, N.y);

        for (int v = 0; v < N.y; v++)
        {

            for (int u = 0; u < N.x; u++)
            {
                cols2D[v * N.x + u] = startColor;
            }
        }
        mat.SetTexture(mapName, tex2D);
        ApplyEdits();
    }
    void ApplyEdits()
    {
        tex2D.SetPixels32(cols2D);
        tex2D.Apply();
    }

    public void PaintTex2D(Vector2 uv)
    {
        int x = Mathf.RoundToInt(uv.x * N.x);
        int y = Mathf.RoundToInt(uv.y * N.y);

        float sqDist;
        float pixBrushSize = brushSize_256 * dimention / 256.0f;

        for (int v = 0; v < N.y; v++)
        {

            for (int u = 0; u < N.x; u++)
            {
                sqDist = (u - x) * (u - x) + (v - y) * (v - y);
                if (sqDist < pixBrushSize * pixBrushSize)
                {
                    cols2D[v * N.x + u] = paintedColor;
                }
            }
        }
        ApplyEdits();
    }
}
