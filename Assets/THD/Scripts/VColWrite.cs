﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
public class VColWrite : MonoBehaviour
{

    public Mesh checkMesh;
    public List<Mesh> inputs;
    public string saveName="mesh1";
    Mesh mesh_out;
    // Start is called before the first frame update
    public void Execute()
    {
        float maxX=0;
        float maxZ=0;
        float minX=0;
        float minZ=0;
        for (int m = 0; m < inputs.Count; m++)
        {
            Mesh mesh_in = inputs[m];
            if (mesh_in)
            {
                mesh_out = Instantiate(mesh_in);
                int N = mesh_out.vertexCount;

                //Color[] cols = new Color[N];
                Vector2[] uv1 = new Vector2[N];
                //Vector2[] uv2 = new Vector2[N];
                Vector3[] verts = mesh_out.vertices;
                for(int i=0;i< N; i++)
                {
                    Vector3 vrt = verts[i];
                    if (vrt.x < minX) minX = vrt.x;
                    if (vrt.x > maxX) maxX = vrt.x;
                    if (vrt.z < minZ) minZ = vrt.z;
                    if (vrt.z > maxZ) maxZ = vrt.z;
                    Vector2 uv_i = new Vector2(vrt.x + 0.5f, vrt.z+0.5f);
                    //Color c_i = new Color(0,1,1, 1);

                    //if (uv_i.x < minX) minX = uv_i.x;
                    //if (uv_i.x > maxX) maxX = uv_i.x;
                    //if (uv_i.y < minZ) minZ = uv_i.y;
                    //if (uv_i.y > maxZ) maxZ = uv_i.y;
                    //Color c_i = new Color(vrt.x,vrt.y,vrt.z,1);
                    uv1[i] = uv_i;
                    //cols[i] = c_i;
                }
                mesh_out.SetUVs(1,uv1);
                //mesh_out.SetColors(cols);
                mesh_out.MarkModified();


                string savePath =  string.Format("Assets/{0}_{1}.asset",saveName,m);
                Debug.Log("Saved Mesh to:" + savePath);
                AssetDatabase.CreateAsset(mesh_out, savePath);
            }
        }
        Debug.LogFormat("x:{0}/{1}   z:{0}/{1}",minX,maxX,minZ,maxZ);
        AssetDatabase.Refresh();
    }

    public void Check()
    {
        for (int v = 0; v < checkMesh.colors.Length; v++)
        {
            Debug.Log(checkMesh.vertices[v]);
        }
    }
}
[CustomEditor(typeof(VColWrite))]
public class VColWriteEditor : Editor
{
    public override void OnInspectorGUI()
    {
        VColWrite vcw = (VColWrite)target;
        base.OnInspectorGUI();
        if (GUILayout.Button("Execute"))
        {
            vcw.Execute();
        }   
        if (GUILayout.Button("Check"))
        {
            vcw.Check();
        }
    }
}
#endif