﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexPaint : MonoBehaviour
{
    public LayerMask layerMaskP;
    public LayerMask layerMaskS;
    public LayerMask layerMaskPS;
    public Camera mainCamera;
    public TexturePaint2D tp2D;
    public TexturePaint3D tp3D;
    //public Material mat0;
    //public Material mat1;
    //public Texture2D tex2D;
    //public Color32 hairBGColor;
    //public Color32 scalpColor;

    //Color32[] cols2D;
    //Color32[] cols3D;

    //[Header("3D")]
    //Texture3D tex3D;
    //int N = 96;
    //int G = 96;
    public bool paintOnUpdate = false;
    //public float hairheight;
    void Start()
    {
        
    }

    Camera cam;

    List<Vector3> woldPoints = new List<Vector3>();
    void Update()
    {
        if (paintOnUpdate)
        {
            if (Input.GetMouseButton(0))
            {
                cam = Camera.main;
                Ray r = cam.ScreenPointToRay(Input.mousePosition);
                PaintTexture(r.origin, r.direction);
            }
            tp3D.UpdatePositionData();
        }
    }
    public void Init(BoxCollider paintBox, Material tp2dMat, Material tp3dMat)
    {
        tp3D.paintBoxReference = paintBox;
        tp2D.mat = tp2dMat;
        tp3D.mat = tp3dMat;
        tp2D.Init();
        tp3D.Init();
    }

    public void PaintTexture(Vector3 rayOrigin, Vector3 rayDiderction)
    {
        RaycastHit[] rcha;
        RaycastHit rch;
        RaycastHit rch2;
        woldPoints.Clear();
        if (Input.GetMouseButton(0))
        {
            if (!cam) cam = mainCamera;

            if (cam)
            {
                /*
                Vector2 mp = Input.mousePosition;
                //Ray r = cam.ScreenPointToRay(new Vector3(mp.x, mp.y, 0));
                Ray r = new Ray(rayOrigin, rayDiderction);
                if (Physics.Raycast(r.origin, r.direction, out rch, 200, layerMaskP))
                {
                    //Debug.DrawLine(r.origin, rch.point, Color.green, 20f);
                    GameObject hitGo = rch.collider.gameObject;
                    //Debug.Log(hitGo + " : " + hitGo.name);
                    if (hitGo.transform.CompareTag("scalp"))
                    {
                        tp2D.PaintTex2D(rch.textureCoord);
                        //tp3D.PaintTex3D(rch.point + rch.normal*hairheight);
                    }
                }

                rcha = Physics.RaycastAll(r.origin, r.direction, 200, layerMaskPS);

                for (int i = 0; i < rcha.Length; i++)
                {
                    woldPoints.Add(rcha[i].point);
                }
                if (woldPoints.Count > 0)
                {
                    tp3D.PaintTex3D(woldPoints);
                }*/

                Vector2 mp = Input.mousePosition;
                //Ray r = cam.ScreenPointToRay(new Vector3(mp.x, mp.y, 0));
                Ray r = new Ray(rayOrigin, rayDiderction);
                if (Physics.Raycast(r.origin, r.direction, out rch, 200, layerMaskP))
                {
                    //Debug.DrawLine(r.origin, rch.point, Color.green, 20f);
                    GameObject hitGo = rch.collider.gameObject;
                    //Debug.Log(hitGo + " : " + hitGo.name);
                    tp2D.PaintTex2D(rch.textureCoord);
                    //tp3D.PaintTex3D(rch.point + rch.normal*hairheight);
                    tp3D.PaintTex3D(rch.point, rch.point + r.direction.normalized * 0.1f);
                    //if (Physics.Raycast(r.origin, r.direction, out rch2, 200, layerMaskS))
                    //{
                    //}
                }

                //rcha = Physics.RaycastAll(r.origin, r.direction, 200, layerMaskPS);

                //for (int i = 0; i < rcha.Length; i++)
                //{
                //    woldPoints.Add(rcha[i].point);
                //}
                //if (woldPoints.Count > 0)
                //{
                //    tp3D.PaintTex3D(woldPoints);
                //}
                
            }
        }
        tp3D.UpdatePositionData();
    }
    //public void PaintTex2D(float brushSize, Vector2 uv)
    //{
    //    Debug.Log(uv);
    //    int x = Mathf.RoundToInt( uv.x * N);
    //    int y = Mathf.RoundToInt( uv.y * N);

    //    float sqDist;
    //    Color c;
    //    for (int i = 0; i < N; i++)
    //    {

    //        for (int j = 0; j < N; j++)
    //        {
    //            sqDist = (j - x) * (j - x) + (i - y) * (i - y);
    //            if (sqDist < brushSize * brushSize)
    //            {
    //                cols2D[i * N + j] = scalpColor;
    //            }
    //        }
    //    }
    //}


    //int Conv(float v)
    //{
    //    return Mathf.RoundToInt(
    //        ((v + (spaceScaleFactor / 2)) / spaceScaleFactor) * G
    //        );
    //}
    //public void PaintTex3D(float brushSize, Vector3 uv)
    //{
    //    Debug.Log(uv);
    //    int x = Conv(uv.x);
    //    int y = Conv(uv.y);
    //    int z = Conv(uv.z);

    //    float sqDist;
    //    for (int i = 0; i < G; i++)
    //    {
    //        for (int j = 0; j < G; j++)
    //        {
    //            for (int k = 0; k < G; k++)
    //            {
    //                sqDist = (k - x) * (k - x) + (j - y) * (j - y) + (i-z)*(i-z);
    //                if (sqDist < brushSize * brushSize)
    //                {
    //                    cols3D[i * G * G + j * G + k] = scalpColor;
    //                }
    //            }
    //        }
    //    }
    //}

    //void ApplyEdits()
    //{
    //    //tex2D.SetPixels32(cols2D);
    //    //tex2D.Apply(); 
        
    //    tex3D.SetPixels32(cols3D);
    //    tex3D.Apply();
    //}

}
