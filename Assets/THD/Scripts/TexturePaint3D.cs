﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TexturePaint3D
{

    public Transform targetTransform;
    public Material mat;
    public float pixPerUnit = 10;
    public float worldBrushSize = 1;
    
    public Color32 startColor = new Color32(1,1,1,1);
    public Color32 paintedColor = new Color32(0,0,0,0);

    public string mapName = "_ScalpMap";
    public string colliderPosVar = "_RefColliderPosition";
    public string colliderSizeVar = "_RefColliderSize";
    public string rotVar = "_Rots";

    public BoxCollider paintBoxReference;
    public Vector3 GetBoxPos()
    {
        return paintBoxReference.center + paintBoxReference.transform.position;
    }
    public Vector3 GetBoxSize()
    {
        return paintBoxReference.size;
    }

    //public Vector3 spaceScaleFactor = new Vector3(1,1,1);

    [Header("Spec")]
    public Vector3Int N;
    public Texture3D tex3D;
    Color32[] cols3D;

    public void Init()
    {
        if (paintBoxReference.transform.lossyScale != Vector3.one)
        {
            Debug.LogError("Check paint box rotation/scale", paintBoxReference.gameObject);
            Debug.LogError("Rot: " + paintBoxReference.gameObject.transform.rotation + " / "+ "Scale: " + paintBoxReference.gameObject.transform.lossyScale);
            Debug.LogError("RotEul: " + paintBoxReference.gameObject.transform.eulerAngles + " / "+ "Scale: " + paintBoxReference.gameObject.transform.lossyScale);
        }
        Vector3 pixSize = GetBoxSize()*pixPerUnit;
        N = new Vector3Int(Mathf.RoundToInt(pixSize.x), Mathf.RoundToInt(pixSize.y), Mathf.RoundToInt(pixSize.z));
        cols3D = new Color32[N.x * N.y*N.z];
        tex3D = new Texture3D(N.x, N.y, N.z, TextureFormat.RGBA32, false);


        for (int w = 0; w < N.z; w++)
        {
            for (int v = 0; v < N.y; v++)
            {
                for (int u = 0; u < N.x; u++)
                {
                    cols3D[(w * N.y * N.x) + (v * N.x) + u] = startColor;
                }
            }
        }

        mat.SetTexture(mapName, tex3D);
        mat.SetVector(colliderSizeVar, GetBoxSize());
        mat.SetVector(colliderPosVar, GetBoxPos());
        //mat.SetFloat(scaleVarName, spaceScaleFactor.x);
        ApplyEdits();


    }
    public void UpdatePositionData()
    {
        mat.SetVector(colliderPosVar, GetBoxPos());
        mat.SetVector(colliderSizeVar, GetBoxSize());
        mat.SetVector(rotVar, paintBoxReference.transform.rotation.eulerAngles);
    }
    void ApplyEdits()
    {
        tex3D.SetPixels32(cols3D);
        tex3D.Apply();
    }

    float ConvFloat(float v, float spaceScaleFactor, float offset, int N)
    {

        
        return  ((v - offset + (spaceScaleFactor / 2)) / spaceScaleFactor) * N;
    }

    Vector3 MyRotate(Vector3 In, Vector3 u, float Rotation)
    {
        Rotation =  Mathf.PI*Rotation/180;

        float s = Mathf.Sin(Rotation);
        float c = Mathf.Cos(Rotation);
        float one_c = 1.0f - c;

        u = u.normalized;
        
        Vector3 r1 = new Vector3(one_c * u.x * u.x + c,         one_c * u.x * u.y - u.z * s,    one_c * u.z * u.x + u.y * s);
        Vector3 r2 = new Vector3(one_c * u.x * u.y + u.z * s,   one_c * u.y * u.y + c,          one_c * u.y * u.z - u.x * s);
        Vector3 r3 = new Vector3(one_c * u.z * u.x - u.y * s,   one_c * u.y * u.z + u.x * s,    one_c * u.z * u.z + c);

        float x = r1.x * In.x + r1.y * In.y + r1.z * In.z;
        float y = r2.x * In.x + r2.y * In.y + r2.z * In.z;
        float z = r3.x * In.x + r3.y * In.y + r3.z * In.z;

        //float x = r1.x * In.x + r2.x * In.y + r3.x * In.z;
        //float y = r1.y * In.x + r2.y * In.y + r3.y * In.z;
        //float z = r1.z * In.x + r2.z * In.y + r3.z * In.z;

        return new Vector3(x,y,z);
    }


    Vector3 Conv(Vector3 pos, Vector3 offset,Vector3 rot, Vector3 scaleFactor,Vector3Int Dims )
    {

        Vector3 final = pos - offset;
        final = MyRotate(final, new Vector3(0, -1, 0), rot.y);
        final = MyRotate(final, new Vector3(-1, 0, 0), rot.x);
        final = MyRotate(final, new Vector3(0, 0, -1), rot.z);
        final += (scaleFactor / 2);
        final.x = final.x*Dims.x / scaleFactor.x;
        final.y = final.y*Dims.y / scaleFactor.y;
        final.z = final.z*Dims.z / scaleFactor.z;

        return final;
    }
    public void PaintTex3D(Vector3 sPos, Vector3 ePos)
    {

        Vector3 spaceScaleFactor = GetBoxSize();
        Vector3 boxPos = GetBoxPos();
        Vector3 rot = paintBoxReference.transform.rotation.eulerAngles;
        Vector3 sConv = Conv(sPos, boxPos, rot, spaceScaleFactor, N);
        Vector3 eConv = Conv(ePos, boxPos, rot, spaceScaleFactor, N);

        float xS = sConv.x;
        float yS = sConv.y;
        float zS = sConv.z;
        
        float xE = eConv.x;
        float yE = eConv.y;
        float zE = eConv.z;
        
        Vector3 dirV = new Vector3(xE - xS, yE - yS, zE - zS).normalized;

        float b1 = dirV.x;
        float b2 = dirV.y;
        float b3 = dirV.z;

        float a1, a2, a3;
        float s1, s2, s3;

        int count=0;
        float pixBrushSize = worldBrushSize * pixPerUnit;
        float sqDist;
        for (int w = 0; w < N.z; w++)
        {
            for (int v = 0; v < N.y; v++)
            {
                for (int u = 0; u < N.x; u++)
                {
                    a1 = u - xS;
                    a2 = v - yS;
                    a3 = w - zS;
                    s1 = a2 * b3 - b2 * a3;
                    s2 = a3 * b1 - b3 * a1;
                    s3 = a1 * b2 - b1 * a2;

                    sqDist = s1*s1 + s2*s2 + s3*s3;
                    if (sqDist < pixBrushSize * pixBrushSize)
                    {
                        count++;
                        cols3D[(w * N.y * N.x) + (v * N.x) + u] = paintedColor;
                    }
                }
            }
        }
        ApplyEdits();
    }

}
