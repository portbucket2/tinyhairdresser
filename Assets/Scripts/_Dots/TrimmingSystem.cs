﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;

public class TrimmingSystem : JobComponentSystem
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    protected override void OnCreate()
    {
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
    }
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var applicationJob = new ApplicationJob
        {
            lawnMower = GetComponentDataFromEntity<LawnMower>(),
            hair = GetComponentDataFromEntity<HairComponenet>()
        };

        return applicationJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDeps);
    }

    [BurstCompile]
    private struct ApplicationJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<LawnMower> lawnMower;
        public ComponentDataFromEntity<HairComponenet> hair;

        public void Execute(TriggerEvent triggerEvent)
        {
            if (lawnMower.HasComponent(triggerEvent.EntityA))
            {
                if (hair.HasComponent(triggerEvent.EntityB))
                {
                    HairComponenet _hair = hair[triggerEvent.EntityB];
                    _hair.radiansPerSecond = math.radians(90f);
                    hair[triggerEvent.EntityB] = _hair;
                }
            }

            if (lawnMower.HasComponent(triggerEvent.EntityB))
            {
                if (hair.HasComponent(triggerEvent.EntityA))
                {
                    HairComponenet _hair  = hair[triggerEvent.EntityA];
                    _hair.radiansPerSecond = math.radians(90f);
                    hair[triggerEvent.EntityA] = _hair;
                }
            }
        }
    }
}