﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class CompletionUI : MonoBehaviour
{
    [SerializeField] Image completionImage;
    [SerializeField] Image irritaionImage;

    void Start()
    {
        
    }

    public void SetCompletion(int comp, float irri)
    {
        float cc = (float)comp / 100f;
        if (cc > 0.7f)
        {
            cc = 0.95f;
        }

        completionImage.DOFillAmount(cc, 0.4f);
        irritaionImage.DOFillAmount(irri,0.2f);
    }
}
