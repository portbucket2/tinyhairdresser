﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraManager : MonoBehaviour
{
    [Header("Cameras")]
    [SerializeField] CinemachineBrain brainCam;
    [SerializeField] CinemachineVirtualCamera trimmingCam;
    [SerializeField] CinemachineVirtualCamera showCam;
    [SerializeField] CinemachineVirtualCamera walkCam;
    [SerializeField] CinemachineVirtualCamera headCam;
    [SerializeField] CinemachineVirtualCamera trimmingCamLeftScalp;
    [SerializeField] CinemachineVirtualCamera trimmingCamBackScalp;

    [SerializeField] Transform cameraTargetLeftScalp;
    [SerializeField] Transform cameraTargetBackScalp;
    [SerializeField] Vector3 leftOffset;
    [SerializeField] Vector3 backOffset;
    [SerializeField] Animator dollyAnimator;

    [Header("WorldUps")]
    [SerializeField] Transform worldUpForLeftScalp;
    [SerializeField] Transform worldUpForRightScalp;
    [SerializeField] Transform worldUpForBackScalp;
    [Header("Directional light")]
    [SerializeField] Transform directionalLight;
    [SerializeField] Vector3 lightForLeft;
    [SerializeField] Vector3 lightForBack;

    [Header("Misc")]
    [SerializeField] Animator transition;

    int TRANSIT = Animator.StringToHash("play");

    WaitForSeconds WAITSHORT = new WaitForSeconds(0.3f);
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);

    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
    }

    public void TrimmingCameraLeftScalp()
    {
        directionalLight.eulerAngles = lightForLeft;

        trimmingCamLeftScalp.Priority = 3;
        brainCam.m_WorldUpOverride = worldUpForLeftScalp;

        trimmingCamBackScalp.Priority = 1;
        trimmingCam.Priority = 1;
        showCam.Priority = 1;
        walkCam.Priority = 1;
        headCam.Priority = 1;
    }
    public void TrimmingCameraBackScalp()
    {
        dollyAnimator.SetTrigger("play");
        directionalLight.eulerAngles = lightForBack;

        trimmingCamBackScalp.Priority = 3;
        brainCam.m_WorldUpOverride = worldUpForBackScalp;

        trimmingCamLeftScalp.Priority = 1;
        trimmingCam.Priority = 1;
        showCam.Priority = 1;
        walkCam.Priority = 1;
        headCam.Priority = 1;
    }
    public void ShowCamera(TrimPosition pos)
    {
        if (pos == TrimPosition.LEFT)
        {
            showCam.Follow = cameraTargetLeftScalp;
            showCam.LookAt = cameraTargetLeftScalp;
            showCam.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = leftOffset;
        }else if (pos == TrimPosition.BACK)
        {
            showCam.Follow = cameraTargetBackScalp;
            showCam.LookAt = cameraTargetBackScalp;
            showCam.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = backOffset;
        }
        showCam.Priority = 3;
        brainCam.m_WorldUpOverride = null;

        trimmingCamBackScalp.Priority = 1;
        trimmingCamLeftScalp.Priority = 1;
        trimmingCam.Priority = 1;
        walkCam.Priority = 1;
        headCam.Priority = 1;
        StartCoroutine(CustomerLeave());
    }
    public void WalkCamera()
    {
        directionalLight.eulerAngles = lightForLeft;

        walkCam.Priority = 2;

        trimmingCamBackScalp.Priority = 1;
        trimmingCamLeftScalp.Priority = 1;
        trimmingCam.Priority = 1;
        showCam.Priority = 1;
        headCam.Priority = 1;
    }
    public void HeadCamera()
    {
        trimmingCam.Priority = 1;
        showCam.Priority = 1;
        walkCam.Priority = 1;
        headCam.Priority = 2;
        //StartCoroutine(CameraZoomTransition());
        TrimmingCameraLeftScalp();
    }
    public Camera GetmainCam()
    {
        return brainCam.GetComponent<Camera>();
    }
    IEnumerator CameraZoomTransition()
    {
        transition.SetTrigger(TRANSIT);
        yield return WAITHALF;
        //transition.SetTrigger(TRANSIT);
        TrimmingCameraLeftScalp();

    }
    IEnumerator CustomerLeave()
    {
        gameController.Confetti();
        yield return WAITONE;
        //transition.SetTrigger(TRANSIT);
        WalkCamera();
        yield return WAITONE;
        gameController.GetCharacterController().Stand();
        //gameController.GetUI().levelCompletePanel.gameObject.SetActive(true);
        yield return WAITONE;
        yield return WAITHALF;
        gameController.GetCharacterController().WalkOut();
    }
}
