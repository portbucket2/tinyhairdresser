﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameDataSheet", order = 1)]
public class GameDataSheet : ScriptableObject
{
    public string detail;

    public List<LevelData> levelDatas;
    
}


[System.Serializable]
public struct LevelData
{
    public Texture2D design;
    public Texture2D outline;
    public Texture2D display;
    public TrimPosition pos;
    [Range(0, 100)]
    public int minToComplete;
    public Vector3 manualStartPosition;
}
public enum TrimPosition
{
    LEFT,
    BACK,
    RIGHT,
    TOP
}
[System.Serializable]
public struct HairColor
{
    public Color hairColor;
    public Color scalpColor;
}
