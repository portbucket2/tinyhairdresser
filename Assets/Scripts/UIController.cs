﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public VariableJoystick joystick;
    public CompletionUI completionUI;
    public CompletionCheck completionCheck;
    public Button doneButton;
    public Button startButton;
    public GameObject dialoguePanel;
    public Image sampleHairStyle;
    public GameObject topPanel;

    GameController gameController;
    GameManager gameManager;
    AnalyticsController analytics;

    private void Start()
    {
        gameManager = GameManager.GetManager();
        gameController = GameController.GetController();
        analytics = AnalyticsController.GetController();

        startButton.onClick.AddListener(delegate
        {
            gameController.TrimmingMode();
            dialoguePanel.SetActive(false);
            ShowCompletionUI();
            
        });
    }

    public void NextLevel()
    {
        analytics.LevelEnded();
        gameManager.GotoNextStage();
        //SceneManager.LoadScene(0);
    }
    public void ShowDialogue(Texture2D tex)
    {
        sampleHairStyle.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        dialoguePanel.SetActive(true);
    }
    void ShowCompletionUI()
    {
        topPanel.SetActive(true);
    }
}
