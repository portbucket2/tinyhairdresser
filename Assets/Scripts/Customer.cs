﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer : MonoBehaviour
{
    [SerializeField] TexPaint texPaint;
    [SerializeField] SkinnedMeshRenderer bodyRenderer;
    [Header("Particles: ")]
    [SerializeField] ParticleSystem hairLeftParticle;
    [SerializeField] ParticleSystem hairBackParticle;
    [SerializeField] ParticleSystem hairRightParticle;
    [SerializeField] ParticleSystem hairTopParticle;
    [Header("Renderers: ")]
    [SerializeField] SkinnedMeshRenderer renB;
    [SerializeField] SkinnedMeshRenderer renL;
    [SerializeField] SkinnedMeshRenderer renR;
    [SerializeField] SkinnedMeshRenderer renT;
    [Header("Colliders: ")]
    [SerializeField] MeshCollider hairLeftCollider;
    [SerializeField] MeshCollider hairBackCollider;
    [SerializeField] MeshCollider hairRightCollider;
    [SerializeField] MeshCollider hairTopCollider;
    [Header("Outlines: ")]
    [SerializeField] GameObject outlineLeft;
    [SerializeField] GameObject outlineBack;
    [SerializeField] GameObject outlineRight;
    [SerializeField] GameObject outlineTop;
    [Header("Paint Boxes: ")]
    [SerializeField] BoxCollider paintBoxLeft;
    [SerializeField] BoxCollider paintBoxBack;
    [SerializeField] BoxCollider paintBoxright;
    [SerializeField] BoxCollider paintBoxTop;
    [Header("textures: ")]
    [SerializeField] List<Texture2D> characterLooks;

    GameObject outline;
    BoxCollider paintBox;
    ParticleSystem currentHair;
    void Start()
    {
        
    }

    public void SetCustomer(TrimPosition _pos)
    {
        hairLeftParticle.gameObject.SetActive(false);
        hairRightParticle.gameObject.SetActive(false);
        hairTopParticle.gameObject.SetActive(false);
        hairBackParticle.gameObject.SetActive(false);

        
        hairLeftCollider.gameObject.SetActive(false);
        hairBackCollider.gameObject.SetActive(false);
        //hairRightCollider.gameObject.SetActive(false);
        //hairTopCollider.gameObject.SetActive(false);

        outlineLeft.SetActive(false);
        outlineBack.SetActive(false);
        //outlineRight.SetActive(false);
        //outlineTop.SetActive(false);

        if (_pos == TrimPosition.LEFT)
        {
            hairLeftParticle.gameObject.SetActive(true);
            currentHair = hairLeftParticle;
            hairLeftCollider.gameObject.SetActive(true);
            outlineLeft.SetActive(true);
            outline = outlineLeft;
            paintBox = paintBoxLeft;
        }
        else if (_pos == TrimPosition.BACK)
        {
            hairBackParticle.gameObject.SetActive(true);
            currentHair = hairBackParticle;

            hairTopParticle.gameObject.SetActive(true);
            hairTopParticle.Play();

            hairBackCollider.gameObject.SetActive(true);
            outlineBack.SetActive(true);
            outline = outlineBack;
            paintBox = paintBoxBack;
        }
    }
    public GameObject GetOutLine()
    {
        return outline;
    }
    public BoxCollider GetPaintBox()
    {
        return paintBox;
    }
    public void PlayParticle()
    {
        currentHair.Play();
    }
    public Material GetScalpMat(TrimPosition pos)
    {
        Material mat = null;
        if (pos == TrimPosition.LEFT) { mat = renL.material; }
        else if (pos == TrimPosition.BACK) { mat = renB.material; }
        else if (pos == TrimPosition.RIGHT) { mat = renR.material; }
        else if (pos == TrimPosition.TOP) { mat = renT.material; }

        return mat;
    }
    public Material GetParticleMat(TrimPosition pos)
    {
        Material mat = null;
        if (pos == TrimPosition.LEFT) { mat = hairLeftParticle.GetComponent<ParticleSystemRenderer>().material; }
        else if (pos == TrimPosition.BACK) { mat = hairBackParticle.GetComponent<ParticleSystemRenderer>().material; }
        else if (pos == TrimPosition.RIGHT) { mat = hairRightParticle.GetComponent<ParticleSystemRenderer>().material; }
        else if (pos == TrimPosition.TOP) { mat = hairTopParticle.GetComponent<ParticleSystemRenderer>().material; }

        return mat;
    }
    public void ChangeHairColor(HairColor color)
    {
        renL.material.SetColor("_HairBackColor", color.scalpColor);
        renB.material.SetColor("_HairBackColor", color.scalpColor);
        renR.material.SetColor("_HairBackColor", color.scalpColor);
        renT.material.SetColor("_HairBackColor", color.scalpColor);

        hairLeftParticle.GetComponent<ParticleSystemRenderer>().material.SetColor("_Color", color.hairColor);
        hairBackParticle.GetComponent<ParticleSystemRenderer>().material.SetColor("_Color", color.hairColor);
        hairRightParticle.GetComponent<ParticleSystemRenderer>().material.SetColor("_Color", color.hairColor);
        hairTopParticle.GetComponent<ParticleSystemRenderer>().material.SetColor("_Color", color.hairColor);

        RandomCharacter();
    }

    void RandomCharacter()
    {
        int max = characterLooks.Count;
        int rand = Random.Range(0, max);
        bodyRenderer.material.SetTexture("_BaseMap", characterLooks[rand]);
    }
}
