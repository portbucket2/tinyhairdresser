﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;


public class PlayerController : MonoBehaviour
{
    [SerializeField] bool inputEnabled = true;
    [SerializeField] LayerMask layerMaskP;
    [SerializeField] LayerMask layerMaskS;
    [SerializeField] VariableJoystick joystick;
    [SerializeField] Transform mower;
    [SerializeField] float speed = 1f;
    [SerializeField] float lertT = 10f;
    [SerializeField] float checkInterval = 5f;
    [SerializeField] CompletionCheck completionCheck;
    [SerializeField] ParticleSystem hairDebri;
    [SerializeField] Animator hairDresser;
    [SerializeField] Animator lawnMower;
    [Header("Debug section: ")]
    [SerializeField] TexPaint texPaint;
    [SerializeField] bool setPosition;
    [SerializeField] Vector3 startPosition;

    Camera mainCam;
    //NavMeshAgent agent;

    readonly string SCALP = "scalp";
    readonly string HAIR = "hair";
    readonly string IDLE = "idle";
    readonly string WALK = "walk";
    readonly string VICTORY = "victory";

    float horizotal = 0f;
    float vertical = 0f;

    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();

    GameController gameController;
    Vector3 oldPosition;
    float timer = 0;
    private void Awake()
    {
        //agent = GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        gameController = GameController.GetController();
        inputEnabled = false;
        joystick.gameObject.SetActive(false);
        mainCam = gameController.GetCameraManager().GetmainCam();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            hairDebri.Play();
            hairDresser.SetTrigger(WALK);
            lawnMower.SetTrigger(WALK);
        }
        if (Input.GetMouseButton(0))
        {
            if (inputEnabled)
            {
                horizotal = joystick.Horizontal * speed;
                vertical = joystick.Vertical * speed;
                //Vector3 camF = mainCam.transform.forward;
                //Vector3 camR = mainCam.transform.right;
                //camF.y = 0;
                //camR.y = 0;
                //camF = camF.normalized;
                //camR = camR.normalized;
                //Debug.DrawRay(transform.position, camF, Color.green);
                //Debug.DrawRay(transform.position, camR, Color.red);
                //Vector3 temp =  (camF * vertical + camR * horizotal);
                //agent.SetDestination(temp);
                Vector3 rayOrigin = mainCam.transform.position;
                Vector3 rayDirection = transform.position - mainCam.transform.position;

                Vector3 screenPoint = mainCam.WorldToScreenPoint(transform.position) + new Vector3(horizotal, vertical, 0f);

                Ray ray = mainCam.ScreenPointToRay(screenPoint);
                //Ray ray = new Ray(rayOrigin, rayDirection);
                oldPosition = transform.position;
                //Debug.DrawRay(ray.origin, ray.direction, Color.cyan, 20f);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 200f, layerMaskP))  
                {
                    Debug.DrawLine(ray.origin, hit.point, Color.cyan);
                    texPaint.PaintTexture(rayOrigin, rayDirection);
                    Vector3 lookAt = (hit.point - oldPosition) * 5f;
                    transform.rotation = Quaternion.LookRotation(lookAt, hit.normal);
                    //mower.LookAt( lookAt.normalized);
                    Debug.DrawRay(transform.position, lookAt.normalized, Color.green);
                    //transform.forward = hit.point - transform.position;

                    transform.position = hit.point ;// Vector3.Lerp(transform.position, hit.point, Time.deltaTime * lertT);

                    //transform.up = hit.normal;

                    //Debug.DrawRay(transform.position, hit.normal, Color.green);

                    //Vector3 lookAt = Vector3.Cross(-hit.normal, transform.right);
                    //lookAt = lookAt.y < 0 ? -lookAt : lookAt;
                    //transform.rotation = Quaternion.LookRotation(hit.point + lookAt, hit.normal);
                    
                }

                //Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
                //foreach (var hitCollider in hitColliders)
                //{
                //    if (hitCollider.transform.CompareTag(HAIR))
                //    {
                //        hitCollider.gameObject.SetActive(false);
                //    }
                //}
            }
            if (timer > checkInterval)
            {
                //times up
                timer = 0f;
                completionCheck.UpdateCompletion();
            }
            else
            {
                timer += Time.deltaTime;
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            completionCheck.UpdateCompletion();
            hairDebri.Stop();
            hairDresser.SetTrigger(IDLE);
            lawnMower.SetTrigger(IDLE);
        }
    }
    public void DisableInput()
    {
        inputEnabled = false;
        joystick.gameObject.SetActive(false);
    }
    public void EnableInput()
    {
        StartCoroutine(EnableInputRoutine());
    }
    IEnumerator EnableInputRoutine()
    {
        yield return WAITONE;        

        Ray ray = gameController.GetCameraManager().GetmainCam().ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f, 0f));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 200f, layerMaskP))
        {
            //Debug.DrawLine(ray.origin, hit.point, Color.yellow, 30f);
            //Debug.DrawRay(hit.point,hit.normal, Color.red, 30f);
            Debug.LogError("enable input: " + hit.textureCoord);
            transform.position = hit.point;
            transform.up = hit.normal;

            transform.position = startPosition;
        }



        //Texture2D tex;
        //RaycastHit[] hits;
        //hits = Physics.RaycastAll(ray.origin, ray.direction, 200f, layerMaskS);

        //for (int i = 0; i < hits.Length; i++)
        //{
        //    RaycastHit hitt = hits[i];
        //    GetPixelWorldPos(hitt.transform);
        //    Vector2 pixelUV = hitt.textureCoord;
        //    tex = hitt.transform.GetComponent<MeshRenderer>().material.mainTexture as Texture2D;
        //    pixelUV.x *= tex.width;
        //    pixelUV.y *= tex.height;
        //    Debug.LogError("check UV: : " + pixelUV);
        //    if (tex.GetPixel((int)pixelUV.x, (int)pixelUV.y).a > 0.8)
        //    {
        //        Debug.LogError("found dotted line:........ " + pixelUV);
        //        break;
        //    }
        //}
        //transform.position = hitz.point;
        //transform.up = hitz.normal;

        yield return ENDOFFRAME;
        inputEnabled = true;
        joystick.gameObject.SetActive(true);
    }
    public void SetPainter(TexPaint _painter, Color hairColor, Vector3 _startposition)
    {
        startPosition = _startposition;
        hairColor.a = 1;
        texPaint = _painter;
        hairDebri.startColor = hairColor;
        hairDebri.transform.GetChild(0).GetComponent<ParticleSystem>().startColor = hairColor;
    }

    //private Vector3 GetPixelWorldPos(Transform aObj)
    //{
    //    MeshRenderer rend = aObj.GetComponent<MeshRenderer>();
    //    Texture2D mainTex = rend.material.mainTexture as Texture2D;
    //    int w = mainTex.width;
    //    int h = mainTex.height;
    //    Debug.Log("object: " + aObj);
    //    // find color in texture
    //    Color[] colors = mainTex.GetPixels();
    //    int index = -1;
    //    for (int i = 0; i < colors.Length; i++)
    //    {
    //        if (colors[i].a > 0.8f)
    //        {
    //            index = i;
    //            Debug.Log("found dot: " + index);
    //            break;
    //        }
    //    }
    //    if (index == -1)
    //        return Vector3.zero;
    //    Vector2 UV = new Vector2(index % w, index / w);
    //    UvTo3D(UV, aObj);

    //    // Find triangle and get local pos
    //    Vector2 point = new Vector2(index % w, index / w) - new Vector2(0.5f / w, 0.5f / h);
    //    MeshFilter mf = aObj.GetComponent<MeshFilter>();
    //    Mesh mesh = mf.sharedMesh;
    //    Vector2[] uvs = mesh.uv;
    //    Vector3[] verts = mesh.vertices;
    //    int[] tris = mesh.triangles;
    //    //Debug.Log("mesh: " + uvs.Length);
    //    //Debug.Log("verts: " + verts.Length);
    //    for (int i = 0; i < verts.Length; i += 3)
    //    {
    //        Vector2 uv0 = uvs[tris[i]];
    //        Vector2 uv1 = uvs[tris[i + 1]];
    //        Vector2 uv2 = uvs[tris[i + 2]];
    //        Vector3 bary = GetBarycentric(uv0, uv1, uv2, point);

            
    //        if (InTriangle(bary))
    //        {
    //            Vector3 localPos = verts[i + 0] * bary.x + verts[i + 1] * bary.y + verts[i + 2] * bary.z;
    //            // Transform to world pos and return
    //            Debug.LogError("found world pos: " +localPos);
    //            return aObj.TransformPoint(localPos);                
    //        }

    //    }
    //    return Vector3.zero;
    //}

    //Vector3 GetBarycentric(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 p)
    //{
    //    Vector3 B = new Vector3();
    //    B.x = ((v2.y - v3.y) * (p.x - v3.x) + (v3.x - v2.x) * (p.y - v3.y)) /
    //        ((v2.y - v3.y) * (v1.x - v3.x) + (v3.x - v2.x) * (v1.y - v3.y));
    //    B.y = ((v3.y - v1.y) * (p.x - v3.x) + (v1.x - v3.x) * (p.y - v3.y)) /
    //        ((v3.y - v1.y) * (v2.x - v3.x) + (v1.x - v3.x) * (v2.y - v3.y));
    //    B.z = 1 - B.x - B.y;
    //    return B;
    //}
    //bool InTriangle(Vector3 barycentric)
    //{
    //    return (barycentric.x >= 0.0f) && (barycentric.x <= 1.0f)
    //        && (barycentric.y >= 0.0f) && (barycentric.y <= 1.0f)
    //        && (barycentric.z >= 0.0f); //(barycentric.z <= 1.0f)
    //}



    //private Vector3 UvTo3D(Vector2 uv, Transform aObj)
    //{
    //    Mesh mesh = aObj.GetComponent<MeshFilter>().mesh;
    //    int[] tris = mesh.triangles;
    //    Vector2[] uvs = mesh.uv;
    //    Vector3[] verts = mesh.vertices;
    //    for (int i = 0; i < tris.Length; i += 3)
    //    {
    //        Vector2 u1 = uvs[tris[i]]; // get the triangle UVs
    //        Vector2 u2 = uvs[tris[i + 1]];
    //        Vector2 u3 = uvs[tris[i + 2]];
    //        // calculate triangle area - if zero, skip it
    //        float a = Area(u1, u2, u3); if (a == 0) continue;
    //        // calculate barycentric coordinates of u1, u2 and u3
    //        // if anyone is negative, point is outside the triangle: skip it
    //        float a1 = Area(u2, u3, uv) / a; if (a1 < 0) continue;
    //        float a2= Area(u3, u1, uv) / a; if (a2 < 0) continue;
    //        float a3= Area(u1, u2, uv) / a; if (a3 < 0) continue;
    //        // point inside the triangle - find mesh position by interpolation...
    //        Vector3 p3D = a1 * verts[tris[i]] + a2 * verts[tris[i + 1]] + a3 * verts[tris[i + 2]];
    //        // and return it in world coordinates:
    //        Debug.Log("location: " + p3D);
    //        return transform.TransformPoint(p3D);

    //    }
    //    // point outside any uv triangle: return Vector3.zero
    //    return Vector3.zero;
    //}

    //// calculate signed triangle area using a kind of "2D cross product":
    //float Area(Vector2 p1, Vector2 p2, Vector2 p3){
    // Vector2 v1 = p1 - p3;
    //Vector2 v2 = p2 - p3;
    //return (v1.x* v2.y - v1.y* v2.x)/2;
    //}

    //void riangleTest(Transform _aObj)
    //{
    //    int[] tris = _aObj.GetComponent<MeshFilter>().mesh.triangles;
    //    tris[0].
    //}
}
