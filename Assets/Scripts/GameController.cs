﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;

    [SerializeField] PlayerController player;
    [SerializeField] CameraManager cameraManager;
    [SerializeField] CharacterController characterController;
    [SerializeField] UIController uiController;
    [SerializeField] ParticleSystem confetti;


    [Header("Level change stuff::")]
    [SerializeField] GameDataSheet data;
    [SerializeField] CompletionCheck checker;
    [SerializeField] Customer customer;
    [Header("Hair colors ")]
    [SerializeField] List<HairColor> hairColors;

    [Header("Debug: ")]
    [SerializeField] int currentLevelIndex;
    [SerializeField] Texture2D currentDesign;
    [SerializeField] Texture2D currentOutline;
    [SerializeField] TrimPosition trimPosition;
    [SerializeField] MeshRenderer outline;
    [SerializeField] TexPaint currentPainter;
    [SerializeField] int minimumP;
    [SerializeField] HairColor CurrentHairColor;

    GameManager gameManager;
    AnalyticsController analytics;


    private void Awake()
    {
        gameController = this;
    }
    private void Start()
    {
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();
        ChooseHair();
        LoadLevelData();
        CustomerEntry();

        checker.Init(currentDesign, currentPainter, minimumP);
        //outline.material.SetTexture("_Texture", currentDesign);
        outline.material.SetTexture("_BaseMap", currentOutline);
    }
    public static GameController GetController() { return gameController; }
    public PlayerController GetPlayer() { return player; }
    public CameraManager GetCameraManager() { return cameraManager; }
    public TexPaint Getpainter() { return currentPainter; }
    public UIController GetUI() { return uiController; }
    public CharacterController GetCharacterController() { return characterController; }
    public Customer GetCustomer() { return customer; }

    private Texture2D showHairStyle;
    public void CustomerEntry()
    {
        cameraManager.WalkCamera();
        analytics.LevelStarted();
        //Invoke("TrimmingMode", 4f);
    }
    public void TrimmingMode()
    {
        if (trimPosition == TrimPosition.LEFT)
        {
            cameraManager.TrimmingCameraLeftScalp();
        }
        else if (trimPosition == TrimPosition.BACK)
        {
            cameraManager.TrimmingCameraBackScalp();
        }

        player.EnableInput();
    }
    public void ShowHair()
    {
        cameraManager.ShowCamera(trimPosition);
    }
    public void CustomerExit()
    {
        cameraManager.WalkCamera();
    }
    public void Confetti()
    {
        confetti.Play();
    }
    public Texture2D GetShowHairStyle()
    {
        return showHairStyle;
    }
    public void SetPlayerStartPositionManually()
    {
        //data.levelDatas[currentLevelIndex].manualStartPosition = transform.position;
    }
    void LoadLevelData()
    {
        int max = data.levelDatas.Count;
        //int rand = Random.Range(0, max);
        int rand = gameManager.GetlevelCount();
        if(currentLevelIndex >= 0)
        {
            rand = currentLevelIndex;
        }
        currentDesign = data.levelDatas[rand].design;
        currentOutline = data.levelDatas[rand].outline;
        trimPosition = data.levelDatas[rand].pos;
        //leftPainter.gameObject.SetActive(false);
        //backPainter.gameObject.SetActive(false);
        customer.SetCustomer(trimPosition);
        currentPainter = customer.GetComponent<TexPaint>();

        //if (trimPosition == TrimPosition.BACK)
        //{
        //    backPainter.gameObject.SetActive(true);
        //    currentPainter = backPainter;
        //}
        //else if(trimPosition == TrimPosition.LEFT)
        //{
        //    leftPainter.gameObject.SetActive(true);
        //    currentPainter = leftPainter;
        //}

        outline = customer.GetOutLine().GetComponent<MeshRenderer>();
        Material scalpMat = customer.GetScalpMat(trimPosition);
        Material particleMat = customer.GetParticleMat(trimPosition);

        customer.ChangeHairColor(CurrentHairColor);

        //scalpMat.SetColor("_HairBackColor", CurrentHairColor.scalpColor);
        //particleMat.SetColor("_Color", CurrentHairColor.hairColor);

        currentPainter.Init(customer.GetPaintBox(), scalpMat, particleMat);

        player.SetPainter(currentPainter, CurrentHairColor.hairColor, data.levelDatas[rand].manualStartPosition);

        minimumP = data.levelDatas[rand].minToComplete;
        showHairStyle = data.levelDatas[rand].display;

    }
    void ChooseHair()
    {
        int max = hairColors.Count;
        int rand = Random.Range(0, max);
        CurrentHairColor = hairColors[rand];
    }


}
