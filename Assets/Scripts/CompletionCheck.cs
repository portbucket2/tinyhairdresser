﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CompletionCheck : MonoBehaviour
{
    
    public Image displayImage;
    public TextMeshProUGUI completedPercent;
    public TextMeshProUGUI irritedPercent;
    public TexPaint texPaint;
    [SerializeField] PlayerController playerController;
    [SerializeField] CompletionUI completionUI;
    [SerializeField] GameObject doneButtonGameobject;
    //[SerializeField] GameObject levelCompletePanel;
    [SerializeField] Button doneButton;
    [Range(0f, 6f)]
    [SerializeField] float irritationThreshhold;

    

    [Header("Debug Section: ")]
    public Texture2D sourceTex;
    [SerializeField] Texture2D drawnTexture;
    [SerializeField] TexturePaint2D drawnTex;
    [SerializeField] int minimumPercent = 80;
    public List<int> onTheLine;
    public List<int> safeZone;

    GameController gameController;

    Color[] allPix;

    void Start()
    {
        gameController = GameController.GetController();
        doneButton.onClick.AddListener(delegate {
            doneButtonGameobject.SetActive(false);
            doneButton.interactable = false;
            //levelCompletePanel.SetActive(true);
            playerController.DisableInput();
            gameController.GetPlayer().gameObject.SetActive(false);
            gameController.ShowHair();
            Invoke("DisableButton", 0.5f);
        });

        onTheLine = new List<int>();
        safeZone = new List<int>();
        //if (sourceTex)
        //{
        //    Init();
        //}
    }
    private void Update()
    {
        
    }
    public void Init(Texture2D _source , TexPaint _painter, int _minimumP)
    {
        minimumPercent = _minimumP;
        texPaint = _painter;
        drawnTex = texPaint.tp2D;

        sourceTex = _source;
        int max = sourceTex.GetPixels().Length;
        allPix = new Color[max];
        //Debug.LogError("initing: ");
        allPix = sourceTex.GetPixels();        

        for (int i = 0; i < max; i++)
        {
            if (allPix[i].r >= 0.5f)
            {
                // out of line
                onTheLine.Add(i);
            }
            if (allPix[i].g <= 0.5f)
            {
                // on line
                safeZone.Add(i);
            }
        }
        //Display();
    }
    void Display()
    {
        int u = Mathf.FloorToInt( sourceTex.width);
        int v = Mathf.FloorToInt( sourceTex.height);

        Texture2D tex = new Texture2D(u, v);
        int max = sourceTex.GetPixels().Length;
        Color[] pix = new Color[max];
        for (int i = 0; i < max; i++)
        {
            pix[i].a = 0;
        }
        int N = onTheLine.Count;
        for (int i = 0; i < N; i++)
        {
            pix[onTheLine[i]].a = 1;
        }
        tex.SetPixels(pix);
        tex.Apply();

        displayImage.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

    }

    public void UpdateCompletion()
    {
        drawnTexture = drawnTex.tex2D;

        displayImage.sprite = Sprite.Create(drawnTexture, new Rect(0.0f, 0.0f, drawnTexture.width, drawnTexture.height), new Vector2(0.5f, 0.5f), 100.0f);

        CheckCompletion();
    }
    void CheckCompletion()
    {
        Color32[] pix = drawnTex.cols2D;

        int max = onTheLine.Count;
        //Debug.Log("onthe line count: " + max);
        //Debug.Log("pix count: " + pix.Length);
        int pixCount = 0;
        int safecount = 0;
        int totalCount = 0;
        int DangerCount = 0;
        for (int i = 0; i < max; i++)
        {
            if (pix[onTheLine[i]].a == 0)
            {
                // on line
                pixCount++;
            }
        }
        int maxx = safeZone.Count;
        for (int i = 0; i < maxx; i++)
        {
            if (pix[safeZone[i]].a == 0)
            {
                // on line
                safecount++;
            }
        }
        int total = pix.Length;
        for (int i = 0; i < total; i++)
        {
            if (pix[i].a == 0)
            {
                // on line
                totalCount++;
            }
        }
        DangerCount = totalCount - safecount;
        float ff = (float)DangerCount / total;
        float irri = ff * 100f;
        irritedPercent.text = "Irrited: " + (int)irri + "%";
        //Debug.LogError("onllCount: " + pixCount);
        //Debug.LogError("danger Count: " + DangerCount);

        if (pixCount > 0)
        {
            float pp = (float)pixCount / max;
            int perc = (int)(pp * 100);
            completedPercent.text = "Completed: " + perc +"%";

            if (perc > minimumPercent)
            {
                //Debug.LogError("Level Complete");
                doneButtonGameobject.SetActive(true);
            }

            irri = (float)irri / irritationThreshhold;

            completionUI.SetCompletion(perc, irri);
        }
    }
    void DisableButton()
    {
        doneButtonGameobject.SetActive(false);
    }
}
