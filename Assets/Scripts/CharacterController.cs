﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterController : MonoBehaviour
{
    [SerializeField] Animator walkerAnimator;

    [SerializeField] Vector3 startpos;
    [SerializeField] Vector3 chairPosition;
    [SerializeField] Vector3 rotateRight;

    [SerializeField] public Transform leftCamtarget;
    [SerializeField] public Transform BackCamtarget;

    Transform walker;
    GameController gameController;
    int WALK = Animator.StringToHash("walk");
    int SIT = Animator.StringToHash("sit");
    int HAPPY = Animator.StringToHash("happy");
    int SAD = Animator.StringToHash("sad");

    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);

    void Start()
    {
        
        gameController = GameController.GetController();
        walker = walkerAnimator.transform;
        startpos = walker.transform.localPosition;

        WalkToChair();
    }

    void WalkToChair() { walkerAnimator.SetTrigger(WALK); walker.DOLocalMove(chairPosition,1f).SetEase(Ease.Linear).OnComplete(TurnRight); }
    void TurnRight() { walker.DORotate(walker.eulerAngles + rotateRight, 0.3f).SetEase(Ease.Linear).OnComplete(Sit); }    
    void Sit() { StartCoroutine(SittingRoutine()); }

    public void Stand() { walkerAnimator.SetTrigger(HAPPY); }
    public void WalkOut() { TurnRightOut();  }
    void TurnRightOut() { walker.DORotate(walker.eulerAngles + rotateRight, 0.3f).SetEase(Ease.Linear).OnComplete(WalkingOut); }
    void WalkingOut() { walkerAnimator.SetTrigger(WALK); walker.DOLocalMove(startpos, 1f).SetEase(Ease.Linear).OnComplete(LevelEnd); }
    void LevelEnd() { gameController.GetUI().NextLevel(); }


    IEnumerator SittingRoutine()
    {
        walkerAnimator.SetTrigger(SIT);
        yield return WAITONE;
        gameController.GetCustomer().PlayParticle();
        // turn on the ui
        gameController.GetUI().ShowDialogue(gameController.GetShowHairStyle());
        
    }
}
