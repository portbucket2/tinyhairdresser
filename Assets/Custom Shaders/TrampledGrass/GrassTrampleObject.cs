﻿// MIT License

// Copyright (c) 2020 NedMakesGames

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class GrassTrampleObject : MonoBehaviour
{
    [Tooltip("The renderer settings with the grass trample feature")]
    [SerializeField] private ForwardRendererData rendererSettings = null;

    private bool TryGetFeature(out GrassTrampleFeature feature)
    {
        // Get the grass trample feature from the settings variable
        feature = rendererSettings.rendererFeatures.OfType<GrassTrampleFeature>().FirstOrDefault();
        return feature != null;
    }

    // Add this object to the tracked list when enabled
    private void OnEnable()
    {
        if (TryGetFeature(out var feature))
        {
            feature.AddTrackedTransform(transform);
        }
    }


    // ... and remove it when disabled
    private void OnDisable()
    {
        if (TryGetFeature(out var feature))
        {
            feature.RemoveTrackedTransform(transform);
        }
    }
}