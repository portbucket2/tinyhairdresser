﻿// MIT License

// Copyright (c) 2020 NedMakesGames

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class GrassTrampleFeature : ScriptableRendererFeature
{
    class Pass : ScriptableRenderPass
    {

        private Vector4[] tramplePositions;
        private int numTramplePositions;

        public Pass(Vector4[] tramplePositions)
        {
            this.tramplePositions = tramplePositions;
        }

        public int NumTramplePositions { get => this.numTramplePositions; set => this.numTramplePositions = value; }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get("GrassTrampleFeature");

            // Set a global array of tracked positions
            cmd.SetGlobalVectorArray("_GrassTramplePositions", tramplePositions);
            // The array length is constant, so this integer will tell the shader how many trample
            // points are valid
            cmd.SetGlobalInt("_NumGrassTramplePositions", numTramplePositions);

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }
    }

    // The maximum number of trample points to send to the shader
    // It should match the _GrassTramplePositions length in the hlsl file
    [SerializeField] private int maxTrackedTransforms = 8;

    private Pass pass;
    private List<Transform> trackingTransforms; // This holds registered transform objects
    private Vector4[] tramplePositions; // This holds saved positions from the registered transforms

    public void AddTrackedTransform(Transform transform)
    {
        trackingTransforms.Add(transform);
    }

    public void RemoveTrackedTransform(Transform transform)
    {
        trackingTransforms.Remove(transform);
    }

    public override void Create()
    {
        trackingTransforms = new List<Transform>();
        // Add transforms to the tracked array which enabled before this feature was created
        trackingTransforms.AddRange(FindObjectsOfType<GrassTrampleObject>().Select((o) => o.transform));
        // Initialize the position array
        tramplePositions = new Vector4[maxTrackedTransforms];
        // Pass the array to the Pass class
        pass = new Pass(tramplePositions);
        // This should run before any rendering so any shaders can use the trample point information
        pass.renderPassEvent = RenderPassEvent.BeforeRendering;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
#if UNITY_EDITOR
        // In the editor, if we switch scenes, an object might not be able to remove itself from being tracked
        // It would then show up as null here. Remove null entries
        trackingTransforms.RemoveAll((t) => t == null);
#endif
        // First clear out all positions
        for (int i = 0; i < tramplePositions.Length; i++)
        {
            tramplePositions[i] = Vector4.zero;
        }
        // Calculate the number of active tracked transforms, bound by the trample positions capacity
        int count = (int)Mathf.Min(trackingTransforms.Count, tramplePositions.Length);
        for (int i = 0; i < count; i++)
        {
            Vector3 posn = trackingTransforms[i].position;
            // Shaders like working with float4s, so add a 1 at the end
            tramplePositions[i] = new Vector4(posn.x, posn.y, posn.z, 1);
        }
        // Update the count in the pass
        pass.NumTramplePositions = count;

        renderer.EnqueuePass(pass);
    }
}